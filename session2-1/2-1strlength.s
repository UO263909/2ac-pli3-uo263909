	.data

string:	.asciiz "Hello"
result:	.word 0

	.text
main:
	xor r16,r16,r16  ; count

loop:
	lbu r4,string(r16)  ;store string[0] in r4
	beqz r4, end   ;if 0 (end of string) jump end
	daddui r16,r16,1  ;+1 count
	j loop

end:
	sd r17, result(r16)  ;store result 
	halt   ;finish
