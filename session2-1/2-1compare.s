	.data

string1:	.asciiz "Hello"
string2:	.asciiz "Hello2"
result:	.word 0

	.text
main:
	xor r16,r16,r16  ; count for string1
	xor r15,r15,r15  ;count for string2

loop1:
	lbu r4,string1(r16)  ;store string[0] in r4
	beqz r4, loop2   ;if 0 (end of string) jump loop2 
	daddui r16,r16,1  ;+1 count
	j loop1

loop2:
	lbu r5,string2(r15)
	beqz r5,end  ;if 0 (end of string)jmp end
	daddui r15,r15,1  ;count2+1
	j loop2
end:
	slt r8,r16,r15  ;compare
	halt
