#include <stdio.h>

void f(double * pD)
{
   *pD = 4;
}

int main(int argc, char* argv[])
{
    double d;
    double * pD;
    d = 3;
    pD = &d;

    f(pD);
    printf("%f\n", d);
    return 0;
}
