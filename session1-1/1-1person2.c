#include <stdio.h>
#include <string.h>

struct _Person{
	int height;
	char name[30];
	double weight;
};
typedef struct _Person Person;
int main(int argc, char* argv[]){
	Person Peter;
	Peter.height = 175;
	Peter.weight = 70.0;
	strcpy(Peter.name,"Peter");
	
	printf("Name:%s H:%d W:%f",Peter.name ,Peter.height ,Peter.weight);
	return 0;
}
